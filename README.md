# dahai-mvl [![NPM version](https://img.shields.io/npm/v/arr-diff.svg?style=flat)](https://www.npmjs.com/package/dahai-mvl)

> An implementation of Łukasiewicz Trilean Logic in Java and Typescript.  

## Install Java

Install with [maven and others](https://search.maven.org/artifact/uk.dahai.open/mvl/0.0.22/jar)

```sh
<dependency>
  <groupId>uk.dahai.open</groupId>
  <artifactId>mvl</artifactId>
  <version>0.0.22</version>
</dependency>
```

## Install Typescript

Install with [npm](https://www.npmjs.com/):

```sh
$ npm install --save dahai-mvl
```

Install with [yarn](https://yarnpkg.com):

```sh
$ yarn add dahai-mvl
```

Install with [bower](https://bower.io/)

```sh
$ bower install dahai-mvl --save
```

## Usage

Consider the function which returns a boolean to indicate if it rained on a certain date.  

```js

 wasItSunnnyOn( isoDate: string ): boolean 

```

If it was sunny you get a 'true' if not 'false'.

However, what is to be done if there is a data gap?  Or if the date provided is in
the future? Or perhaps if supporting data-service is down?

A null could be returned, or perhaps an exception raised?  A trilean would allow you to return 'unknown' and handle
these possibilities as part of the normal flow of execution:

```js

itWasSunnnyOn( isoDate: string ): Trit;

const battleOfHastings = "1066-10-14";

if ( isTrue( itWasSunnnyOn( battleOfHastings ) ) )
    console.log( 'Great lets party!' );
else if ( isFalse( itWasSunnnyOn( battleOfHastings ) ) )
    console.log( 'Some other time!' );
else if ( isUnknown( itWasSunnnyOn( battleOfHastings ) ) )
    console.log( 'Lets check another service that!' );

```

You can map to and from booleans by using coalesce and booleanValue:

```js


coalesce( null ) === Trilean.U

Trilean.U.booleanValue === null;
isTrue( Trilean.T ) === true;
isFalse( Trilean.F ) === false;
isKnown( Trilean.U ) === false;
isUnknown( Trilean.U ) === true;

```


### Contributing

The project is available on [gitlab](https://gitlab.com/dahaiuk/open) contributions and bug reports 
welcome.

### Running tests

Running and reviewing unit tests is a great way to get familiarized with a library and its API. You can install dependencies and run tests with the following command:

```sh
$ npm install && npm test
```

### Author

**Richard Bourner**

* [linkedin](https://www.linkedin.com/in/rickbourner/)

### License

Copyright © 2017, [Richard Bourner](https://gitlab.com/dahailtd).
Released under the [MIT License](LICENSE).