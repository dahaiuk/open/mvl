package uk.dahai.open.mvl.trilean;

import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public interface TrileanFn
{
    Function<Trilean, Integer> toTernaryValue = Trilean::ternaryValue;
    Function<Trilean, Integer> toBalancedTernaryValue = Trilean::balancedTernaryValue;
    Function<Trilean, Boolean> toBoolean = Trilean::booleanValue;

    // upgrade booleans, dealing with nulls
    Function<Trilean, Trilean> coalesceTrilean = Trilean::coalesce;

    // Bridge back to boring old booleans
    Predicate<Trilean> isTrue = a -> coalesceTrilean.apply( a ).isTrue();
    Predicate<Trilean> isNotTrue = a -> !coalesceTrilean.apply( a ).isTrue();
    Predicate<Trilean> isFalse = a -> coalesceTrilean.apply( a ).isFalse();
    Predicate<Trilean> isNotFalse = a -> !coalesceTrilean.apply( a ).isFalse();
    Predicate<Trilean> isUnknown = a -> coalesceTrilean.apply( a ).isUnknown();
    Predicate<Trilean> isKnown = a -> coalesceTrilean.apply( a ).isKnown();
    Predicate<Trilean> isUnknownOrTrue = a -> coalesceTrilean.apply( a ).isNotFalse();
    Predicate<Trilean> isUnknownOrFalse = a -> coalesceTrilean.apply( a ).isNotTrue();

    Function<Trilean, Trilean> not = a -> coalesceTrilean.apply( a ).not();

    BiFunction<Trilean, Trilean, Trilean> and = ( a, b ) -> coalesceTrilean.apply( a ).and( b );
    Function<Trilean, Function<Trilean, Trilean>> andWith = a -> b -> and.apply( a, b );

    BiFunction<Trilean, Trilean, Trilean> or = ( a, b ) -> coalesceTrilean.apply( a ).or( b );
    Function<Trilean, Function<Trilean, Trilean>> orWith = a -> b -> or.apply( a, b );

    BiFunction<Trilean, Trilean, Trilean> implies = ( a, b ) -> coalesceTrilean.apply( a ).implies( b );
    Function<Trilean, Function<Trilean, Trilean>> itImplies = a -> b -> implies.apply( a, b );

    Function<Trilean, String> toString = ( a ) -> coalesceTrilean.apply( a ).toString();
    Function<String, Trilean> fromString = Trilean::fromString;

    static <T> Function<T, T> when( Function<T, Trilean> truth,
                                    Function<T, T> whenTrue )
    {
        return when( truth, whenTrue, Function.identity(), Function.identity() );
    }

    static <T> Function<T, T> when( Function<T, Trilean> truth,
                                    Function<T, T> whenTrue,
                                    Function<T, T> whenFalse )
    {
        return when( truth, whenTrue, whenFalse, Function.identity() );
    }

    static <T> Function<T, T> when( Function<T, Trilean> truth,
                                    Function<T, T> whenTrue,
                                    Function<T, T> whenFalse,
                                    Function<T, T> whenUnknown )
    {
        return any -> switch ( truth.apply( any ) )
        {
            case T -> whenTrue.apply( any );
            case F -> whenFalse.apply( any );
            default -> // also catches null case
            whenUnknown.apply( any );
        };
    }


    static <T, U> Function<T, U> select( Function<T, Trilean> truth,
                                         U whenTrue,
                                         U whenFalse,
                                         U whenUnknown )
    {
        return any -> switch ( truth.apply( any ) )
        {
            case T -> whenTrue;
            case F -> whenFalse;
            default -> whenUnknown;
        };
    }

    Function<Object, Trilean> coalesce =
    o -> o == null ? Trilean.U
                   : switch ( o.getClass().getSimpleName() )
         {
             case "String" -> Trilean.fromString( (String) o );
             case "Trilean" -> Trilean.coalesce( (Trilean) o );
             case "Boolean" -> Trilean.coalesce( (Boolean) o );
             default -> throw new RuntimeException( "Unable to coalesce trilean from " + o.getClass() );
         };

    static <T extends Map<String, Object>> Predicate<T> isTrue( String key )
    {
        return any -> coalesce.apply( any.get( key ) ).isTrue();
    }

    static <T extends Map<String, Object>> Predicate<T> isFalse( String key )
    {
        return any -> coalesce.apply( any.get( key ) ).isFalse();
    }

    static <T extends Map<String, Object>> Predicate<T> isUnknown( String key )
    {
        return any -> coalesce.apply( any.get( key ) ).isUnknown();
    }

    static <T extends Map<String, Object>> Predicate<T> isKnown( String key )
    {
        return any -> coalesce.apply( any.get( key ) ).isKnown();
    }

    static <T extends Map<String, Object>> Function<T, Trilean> is( String key, Object value )
    {
        return any -> Trilean.equals( value, any.get( key ) );
    }

    static <T extends Map<String, Object>> Function<T, Trilean> isNot( String key, Object value )
    {
        return any -> Trilean.equals( value, any.get( key ) ).not();
    }


    static <T> Predicate<T> when( Function<T, Trilean> test, Predicate<Trilean> is )
    {
        return any -> is.test( test.apply( any ) );
    }

    static <T> Predicate<T> whenTrue( Function<T, Trilean> test )
    {
        return when( test, isTrue );
    }

    static <T> Predicate<T> whenNotTrue( Function<T, Trilean> test )
    {
        return when( test, isNotTrue );
    }

    static <T> Predicate<T> whenFalse( Function<T, Trilean> test )
    {
        return when( test, isFalse );
    }

    static <T> Predicate<T> whenNotFalse( Function<T, Trilean> test )
    {
        return when( test, isNotFalse );
    }

    static <T> Predicate<T> whenKnown( Function<T, Trilean> test )
    {
        return when( test, isKnown );
    }

    static <T> Predicate<T> whenUnknown( Function<T, Trilean> test )
    {
        return when( test, isUnknown );
    }

    static <T> Predicate<T> whenUnknownOrTrue( Function<T, Trilean> test )
    {
        return when( test, isUnknownOrTrue );
    }

    static <T> Predicate<T> whenUnknownOrFalse( Function<T, Trilean> test )
    {
        return when( test, isUnknownOrFalse );
    }


}
