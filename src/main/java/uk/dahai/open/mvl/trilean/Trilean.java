package uk.dahai.open.mvl.trilean;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * An implementation of Lukasiewicz's three value logic.
 * <p>
 * Please note that throughout this implementation null values are mapped to
 * and from U ( unknown ).
 * <p>
 * https://en.wikipedia.org/wiki/Three-valued_logic
 * https://en.wikipedia.org/wiki/%C5%81ukasiewicz_logic
 */
public enum Trilean
{
    F( 0, -1, FALSE, "false" ),
    U( 1, 0, null, "unknown" ),
    T( 2, 1, TRUE, "true" );

    final static List<Trilean> all = List.of( F, U, T );

    private final int ternaryValue;
    private final int balancedTernaryValue;
    private final Boolean booleanValue;
    private final String stringValue;

    Trilean( int ternaryValue, int balancedTernaryValue, Boolean booleanValue, String stringValue )
    {
        this.ternaryValue = ternaryValue;
        this.balancedTernaryValue = balancedTernaryValue;
        this.booleanValue = booleanValue;
        this.stringValue = stringValue;
    }

    public int ternaryValue()
    {
        return ternaryValue;
    }

    /**
     * The sum of balanced ternary values (-1=f,0=u,1=t) can be used to determine the truthiness of
     * a series of trileans.
     * <p>
     * https://en.wikipedia.org/wiki/Balanced_ternary
     */
    public int balancedTernaryValue()
    {
        return balancedTernaryValue;
    }

    /**
     * Bridge back to booleans, mapping U ( Unknown ) to null.
     */
    public Boolean booleanValue()
    {
        return booleanValue;
    }

    /**
     * Map null trileans to U ( Unknown )
     */
    public static Trilean coalesce( Trilean t )
    {
        return t == null ? U : t;
    }

    /**
     * Convert a Boolean to a Trilean, mapping null to U ( Unknown )
     */
    public static Trilean coalesce( Boolean b )
    {
        return ( b == null ) ? U : b ? T : F;
    }

    /**
     * Convert a Boolean to a Trilean, mapping null to U ( Unknown )
     */
    public static Trilean coalesce( String s )
    {
        if ( s == null )
            return Trilean.U;

        s = s.trim().toLowerCase();

        if ( s.equals( "true" ) || s.equals( "t" ) || s.equals( "yes" ) )
            return Trilean.T;
        else if ( s.equals( "false" ) || s.equals( "f" ) || s.equals( "no" ) )
            return Trilean.F;
        else
            return Trilean.U;
    }

    public Boolean isFalse()
    {
        return this == F;
    }

    public Boolean isNotFalse()
    {
        return this != F;
    }

    public Boolean isTrue()
    {
        return this == T;
    }

    public Boolean isNotTrue()
    {
        return this != T;
    }

    public Boolean isKnown()
    {
        return !this.isUnknown();
    }

    public Boolean isUnknown()
    {
        return this == U;
    }

    public Boolean isUnknownOrFalse()
    {
        return this == U || this == F;
    }

    public Boolean isUnknownOrTrue()
    {
        return this == U || this == T;
    }


    public Trilean assumeToBeTrue()
    {
        return this.isUnknown() ? Trilean.T : this;
    }

    public Trilean assumeToBeFalse()
    {
        return this.isUnknown() ? Trilean.F : this;
    }

    public Trilean not()
    {
        return this.isTrue() ? F : this.isFalse() ? T : U;
    }

    public Trilean and( Trilean b )
    {
        b = coalesce( b );

        if ( isFalse() || b.isFalse() )
            return F;
        else if ( isTrue() && b.isTrue() )
            return T;
        else
            return U;
    }

    public Trilean or( Trilean b )
    {
        b = coalesce( b );

        if ( this.isTrue() || b.isTrue() )
            return T;
        else if ( this.isFalse() && b.isFalse() )
            return F;
        else
            return U;
    }

    public Trilean implies( Trilean b )
    {
        b = coalesce( b );

        if ( this.isFalse() || b.isTrue() || ( this.isUnknown() && b.isUnknown() ) )
            return T;
        else if ( this.isUnknown() || b.isUnknown() )
            return U;
        else
            return F;
    }

    public static Trilean fromString( String bs )
    {
        return T.stringValue.equals( bs ) ? T : F.stringValue.equals( bs ) ? F : U;
    }

    @Override
    public String toString()
    {
        return this.stringValue;
    }

    public static Trilean equals( Object a, Object b )
    {
        return a == null || b == null ? Trilean.U : a.equals( b ) ? Trilean.T : Trilean.F;
    }

    public static Trilean notEquals( Object a, Object b )
    {
        return equals( a, b ).not();
    }

    public static Trilean random() {
        var random =  ThreadLocalRandom.current().nextInt( 0, 3);
        return all.get( random );
    }

}
