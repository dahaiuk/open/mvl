export class Trilean
{
    private readonly ternaryValue: number;
    private readonly balancedTernaryValue: number;
    private readonly booleanValue: boolean | undefined;
    private readonly stringValue: string;

    private constructor( ternaryValue: number,
                         balancedTernaryValue: number,
                         booleanValue: boolean | undefined,
                         stringValue: string )
    {
        this.ternaryValue = ternaryValue;
        this.balancedTernaryValue = balancedTernaryValue;
        this.booleanValue = booleanValue;
        this.stringValue = stringValue;
    }

    public static F = new Trilean( 1, -1, false, "false" );
    public static U = new Trilean( 2, 0, undefined, "unknown" );
    public static T = new Trilean( 3, 1, true, "true" );

    public static all = [Trilean.F, Trilean.U, Trilean.T];

    public toBoolean = () => this.booleanValue;
    public toTernary = () => this.ternaryValue;
    public toBalancedTernary = () => this.balancedTernaryValue;
    public toString = () => this.stringValue;

    public isFalse = () => this === Trilean.F;
    public isNotFalse = () => this !== Trilean.F;
    public isTrue = () => this === Trilean.T;
    public isNotTrue = () => this !== Trilean.T;
    public isUnknown = () => this === Trilean.U;
    public isKnown = () => !this.isUnknown();
    public isUnknownOrFalse = () => this.isUnknown() || this.isFalse();
    public isUnknownOrTrue = () => this.isUnknown() || this.isTrue();

    public static coalesce = ( t: Trilean | boolean | undefined | null | string ) => {

        if ( t === undefined || t === null || t as string === "unknown" || t as string === "maybe" || t as Trilean === Trilean.U )
            return Trilean.U;
        else if ( t as string === "T" || t as string === "true" || t as string === 'yes' || t as Trilean === Trilean.T )
            return Trilean.T;
        else if ( t as string === "F" || t as string === "false" || t as string === 'no' || t as Trilean === Trilean.F )
            return Trilean.F;
        else if ( typeof t === "boolean" && t )
            return Trilean.T;
        else if ( typeof t === "boolean" && !t )
            return Trilean.F;
        else
            return Trilean.U;
    }

    public not = () => this.isTrue() ? Trilean.F : this.isFalse() ? Trilean.T : Trilean.U;

    public assumeToBeTrue = () => this.isUnknown() ? Trilean.T : this;
    public assumeToBeFalse = () => this.isUnknown() ? Trilean.F : this;

    public and = ( t: Trilean | boolean | string | undefined | null ) => {

        const that: Trilean = Trilean.coalesce( t );

        if ( this.isFalse() || that.isFalse() )
            return Trilean.F;

        else if ( this.isTrue() && that.isTrue() )
            return Trilean.T;

        else
            return Trilean.U;
    }

    public or = ( t: Trilean | boolean | string | undefined | null ) => {

        const that = Trilean.coalesce( t );

        if ( this.isTrue() || that.isTrue() )
            return Trilean.T;
        else if ( this.isFalse() && that.isFalse() )
            return Trilean.F;
        else
            return Trilean.U;
    }

    public implies = ( t: Trilean | boolean | string | undefined | null ) => {

        const that = Trilean.coalesce( t );

        if ( this.isFalse() || that.isTrue() || ( this.isUnknown() && that.isUnknown() ) )
            return Trilean.T;
        else if ( this.isUnknown() || that.isUnknown() )
            return Trilean.U;
        else
            return Trilean.F;
    }
}

export const equals = ( a: any, b: any ) => a == null || b == null ? Trilean.U : a === b ? Trilean.T : Trilean.F;
export const notEquals = ( a: any, b: any ) => equals( a, b ).not();
export const or = ( a: any, b: any ) => Trilean.coalesce( a ).or( b );
export const and = ( a: any, b: any ) => Trilean.coalesce( a ).and( b );
export const implies = ( a: any, b: any ) => Trilean.coalesce( a ).implies( b );

export const random = () => Trilean.all[ Math.floor( Math.random() * 3 ) ];

// Bridge back to boring old booleans
export const isTrue = ( a: any ) => Trilean.coalesce( a ).isTrue();
export const isNotTrue = ( a: any ) => !Trilean.coalesce( a ).isTrue();
export const isFalse = ( a: any ) => Trilean.coalesce( a ).isFalse();
export const isNotFalse = ( a: any ) => !Trilean.coalesce( a ).isFalse();
export const isUnknown = ( a: any ) => Trilean.coalesce( a ).isUnknown();
export const isKnown = ( a: any ) => Trilean.coalesce( a ).isKnown();
export const isUnknownOrTrue = ( a: any ) => Trilean.coalesce( a ).isNotFalse();
export const isUnknownOrFalse = ( a: any ) => Trilean.coalesce( a ).isNotTrue();




