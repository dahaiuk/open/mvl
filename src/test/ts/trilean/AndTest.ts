import {Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean And Test', function () {

    it( 'hasTheCorrectAndFn', function () {

        expect( Trilean.F.and( Trilean.F ) ).toBe( Trilean.F );
        expect( Trilean.F.and( Trilean.U ) ).toBe( Trilean.F );
        expect( Trilean.F.and( Trilean.T ) ).toBe( Trilean.F );

        expect( Trilean.U.and( Trilean.F ) ).toBe( Trilean.F );
        expect( Trilean.U.and( Trilean.U ) ).toBe( Trilean.U );
        expect( Trilean.U.and( Trilean.T ) ).toBe( Trilean.U );

        expect( Trilean.T.and( Trilean.F ) ).toBe( Trilean.F );
        expect( Trilean.T.and( Trilean.U ) ).toBe( Trilean.U );
        expect( Trilean.T.and( Trilean.T ) ).toBe( Trilean.T );

    } );

    it( 'hasTheCorrectAndCoalesceFn', function () {

        expect(Trilean.F.and( null ) ).toBe( Trilean.F );
        expect(Trilean.F.and( undefined ) ).toBe( Trilean.F );
        expect(Trilean.F.and( "false" ) ).toBe( Trilean.F );
        expect(Trilean.F.and( "lorum ipsum" ) ).toBe( Trilean.F );
        expect(Trilean.F.and( true ) ).toBe( Trilean.F );

    } );
});