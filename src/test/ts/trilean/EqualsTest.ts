import {equals, notEquals, Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean Equals Test', function () {

    it( 'equalsesCorrectly', function () {

        expect( equals( null, null ) ).toBe( Trilean.U );
        expect( equals( null, 10 ) ).toBe( Trilean.U );
        expect( equals( 10, null ) ).toBe( Trilean.U );

        expect( equals( 10, 10 ) ).toBe( Trilean.T );
        expect( equals( 9, 10 ) ).toBe( Trilean.F );

    } );

    it( 'notEequalsesCorrectly', function () {

        expect( notEquals( null, null ) ).toBe( Trilean.U );
        expect( notEquals( null, 10 ) ).toBe( Trilean.U );
        expect( notEquals( 10, null ) ).toBe( Trilean.U );

        expect( notEquals( 10, 10 ) ).toBe( Trilean.F );
        expect( notEquals( 9, 10 ) ).toBe( Trilean.T );

    } );


});