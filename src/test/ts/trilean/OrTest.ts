import {Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean And Test', function () {

    it( 'hasTheCorrectAndFn', function () {

        expect( Trilean.F.or( Trilean.F ) ).toBe( Trilean.F );
        expect( Trilean.F.or( Trilean.U ) ).toBe( Trilean.U );
        expect( Trilean.F.or( undefined ) ).toBe( Trilean.U );
        expect( Trilean.F.or( null ) ).toBe( Trilean.U );
        expect( Trilean.F.or( Trilean.T ) ).toBe( Trilean.T );

        expect( Trilean.U.or( Trilean.F ) ).toBe( Trilean.U );
        expect( Trilean.U.or( Trilean.U ) ).toBe( Trilean.U );
        expect( Trilean.U.or( undefined ) ).toBe( Trilean.U );
        expect( Trilean.U.or( null ) ).toBe( Trilean.U );
        expect( Trilean.U.or( Trilean.T ) ).toBe( Trilean.T );

        expect( Trilean.T.or( Trilean.F ) ).toBe( Trilean.T );
        expect( Trilean.T.or( Trilean.U ) ).toBe( Trilean.T );
        expect( Trilean.T.or( undefined ) ).toBe( Trilean.T );
        expect( Trilean.T.or( null ) ).toBe( Trilean.T );
        expect( Trilean.T.or( Trilean.T ) ).toBe( Trilean.T );

    } );


} );