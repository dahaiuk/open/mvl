import {Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean Ternary Value', function () {

    // @ts-ignore
    it( 'hasTheCorrectTernaryValues', function () {

        expect( Trilean.F.toTernary() ).toEqual( 1 );
        expect( Trilean.U.toTernary() ).toEqual( 2 );
        expect( Trilean.T.toTernary() ).toEqual( 3 );
    } );

    it( 'hasTheCorrectBalancedTernaryValues', function () {

        expect( Trilean.F.toBalancedTernary() ).toEqual( -1 );
        expect( Trilean.U.toBalancedTernary() ).toEqual( 0 );
        expect( Trilean.T.toBalancedTernary() ).toEqual( 1 );
    } );

    it( 'hasTheCorrectBooleanValues', function () {

        expect( Trilean.F.toBoolean() ).toEqual( false );
        expect( Trilean.U.toBoolean()  ).toEqual( undefined );
        expect( Trilean.T.toBoolean()  ).toEqual( true );
    } );

    it( 'hasTheCorrectStringValues', function () {

        expect( Trilean.F.toString() ).toEqual( "false" );
        expect( Trilean.U.toString() ).toEqual( "unknown" );
        expect( Trilean.T.toString() ).toEqual( "true" );
    } );

} );