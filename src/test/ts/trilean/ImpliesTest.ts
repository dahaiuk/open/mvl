import {Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean Implies Test', function () {

    it( 'hasTheCorrectImpliesFn', function () {

        expect( Trilean.F.implies( Trilean.F ) ).toBe( Trilean.T );
        expect( Trilean.F.implies( Trilean.U ) ).toBe( Trilean.T );
        expect( Trilean.F.implies( undefined ) ).toBe( Trilean.T );
        expect( Trilean.F.implies( null ) ).toBe( Trilean.T );
        expect( Trilean.F.implies( Trilean.T ) ).toBe( Trilean.T );

        expect( Trilean.U.implies( Trilean.F ) ).toBe( Trilean.U );
        expect( Trilean.U.implies( Trilean.U ) ).toBe( Trilean.T );
        expect( Trilean.U.implies( undefined ) ).toBe( Trilean.T );
        expect( Trilean.U.implies( null ) ).toBe( Trilean.T );
        expect( Trilean.U.implies( Trilean.T ) ).toBe( Trilean.T );

        expect( Trilean.T.implies( Trilean.F ) ).toBe( Trilean.F );
        expect( Trilean.T.implies( Trilean.U ) ).toBe( Trilean.U );
        expect( Trilean.T.implies( undefined ) ).toBe( Trilean.U );
        expect( Trilean.T.implies( null ) ).toBe( Trilean.U );
        expect( Trilean.T.implies( Trilean.T ) ).toBe( Trilean.T );


    } );


} );