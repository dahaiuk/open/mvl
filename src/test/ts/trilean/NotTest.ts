import {Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean Not Test', function () {

    it( 'hasTheCorrectNotFn', function () {

        expect( Trilean.F.not() ).toBe( Trilean.T );
        expect( Trilean.U.not() ).toBe( Trilean.U );
        expect( Trilean.T.not() ).toBe( Trilean.F );
    } );


} );