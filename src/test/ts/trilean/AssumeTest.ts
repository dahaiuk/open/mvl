import {equals, notEquals, Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean Assumption Test', function () {

    it( 'truthyTest', function () {

        expect( Trilean.U.assumeToBeTrue() ).toBe( Trilean.T );
        expect( Trilean.U.assumeToBeFalse() ).toBe( Trilean.F );

        expect( Trilean.T.assumeToBeTrue() ).toBe( Trilean.T );
        expect( Trilean.T.assumeToBeFalse() ).toBe( Trilean.T );

        expect( Trilean.F.assumeToBeTrue() ).toBe( Trilean.F );
        expect( Trilean.F.assumeToBeFalse() ).toBe( Trilean.F );

    } );

});