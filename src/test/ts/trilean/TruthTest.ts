import {Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean Truth Fn Tests', function () {

    it( 'hasTheCorrectTruthValuesForFalse', function () {

        expect( Trilean.F.isFalse() ).toBeTrue();
        expect( Trilean.U.isFalse() ).toBeFalse();
        expect( Trilean.T.isFalse() ).toBeFalse();
    } );

    it( 'hasTheCorrectTruthValuesForUnknown', function () {

        expect( Trilean.F.isUnknown() ).toBeFalse();
        expect( Trilean.U.isUnknown() ).toBeTrue();
        expect( Trilean.T.isUnknown() ).toBeFalse();
    } );

    it( 'hasTheCorrectTruthValuesForTsTrue', function () {

        expect( Trilean.F.isTrue() ).toBeFalse();
        expect( Trilean.U.isTrue() ).toBeFalse();
        expect( Trilean.T.isTrue() ).toBeTrue();
    } );

    it( 'hasTheCorrectTruthValuesForTsKnown', function () {

        expect( Trilean.F.isKnown() ).toBeTrue();
        expect( Trilean.U.isKnown() ).toBeFalse();
        expect( Trilean.T.isKnown() ).toBeTrue();
    } );

} );