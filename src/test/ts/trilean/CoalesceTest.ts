import {Trilean} from "../../../main/ts/Trilean";

describe( 'Trilean Coalesce Test', function () {

    it( 'hasTheCorrectCoalesceFn', function () {

        expect( Trilean.coalesce( null ) ).toBe( Trilean.U );
        expect( Trilean.coalesce( undefined ) ).toBe( Trilean.U );

        expect( Trilean.coalesce( true ) ).toBe( Trilean.T );
        expect( Trilean.coalesce( false ) ).toBe( Trilean.F );

       expect( Trilean.coalesce( "true" ) ).toBe( Trilean.T );
       expect( Trilean.coalesce( "yes" ) ).toBe( Trilean.T );
       expect( Trilean.coalesce( "false" ) ).toBe( Trilean.F );
       expect( Trilean.coalesce( "no" ) ).toBe( Trilean.F );
       expect( Trilean.coalesce( "unknown" ) ).toBe( Trilean.U );
       expect( Trilean.coalesce( "maybe" ) ).toBe( Trilean.U );
       expect( Trilean.coalesce( "ipsum" ) ).toBe( Trilean.U );

    } );


} );