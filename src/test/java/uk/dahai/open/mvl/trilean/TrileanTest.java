package uk.dahai.open.mvl.trilean;

import org.junit.jupiter.api.Test;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.junit.jupiter.api.Assertions.*;
import static uk.dahai.open.mvl.trilean.Trilean.*;

public class TrileanTest
{

    @Test
    public void ternaryValueTest()
    {
        assertEquals( 0, F.ternaryValue() );
        assertEquals( 1, U.ternaryValue() );
        assertEquals( 2, T.ternaryValue() );
    }

    @Test
    public void balancedTernaryValueTest()
    {
        assertEquals( -1, F.balancedTernaryValue() );
        assertEquals( 0, U.balancedTernaryValue() );
        assertEquals( 1, T.balancedTernaryValue() );
    }

    @Test
    public void booleanValueTest()
    {
        assertEquals( FALSE, F.booleanValue() );
        assertNull( U.booleanValue() );
        assertEquals( TRUE, T.booleanValue() );
    }

    @Test
    public void falsyLogicTest()
    {
        assertTrue( F.isFalse() );
        assertFalse( F.isTrue() );
        assertTrue( F.isKnown() );
        assertFalse( F.isUnknown() );
        assertTrue( F.isNotTrue() );
        assertFalse( F.isNotFalse() );
    }

    @Test
    public void truthyLogicTest()
    {
        assertFalse( T.isFalse() );
        assertTrue( T.isTrue() );
        assertTrue( T.isKnown() );
        assertFalse( T.isUnknown() );
    }

    @Test
    public void unknownLogicTest()
    {
        assertFalse( U.isTrue() );
        assertFalse( U.isTrue() );
        assertFalse( U.isKnown() );
        assertTrue( U.isUnknown() );
    }


    @Test
    public void testNot()
    {
        assertTrue( T.not().isFalse() );
        assertTrue( F.not().isTrue() );
        assertTrue( U.not().isUnknown() );
    }

    @Test
    public void testAnd()
    {
        assertTrue( F.and( F ).isFalse() );
        assertTrue( U.and( F ).isFalse() );
        assertTrue( T.and( F ).isFalse() );

        assertTrue( F.and( U ).isFalse() );
        assertTrue( F.and( null ).isFalse() );
        assertTrue( U.and( U ).isUnknown() );
        assertTrue( U.and( null ).isUnknown() );
        assertTrue( T.and( U ).isUnknown() );
        assertTrue( T.and( null ).isUnknown() );

        assertTrue( F.and( T ).isFalse() );
        assertTrue( U.and( T ).isUnknown() );
        assertTrue( T.and( T ).isTrue() );
    }

    @Test
    public void testOr()
    {
        assertTrue( F.or( F ).isFalse() );
        assertTrue( U.or( F ).isUnknown() );
        assertTrue( T.or( F ).isTrue() );

        assertTrue( F.or( U ).isUnknown() );
        assertTrue( F.or( null ).isUnknown() );
        assertTrue( U.or( U ).isUnknown() );
        assertTrue( U.or( null ).isUnknown() );
        assertTrue( T.or( U ).isTrue() );
        assertTrue( T.or( null ).isTrue() );

        assertTrue( F.or( T ).isTrue() );
        assertTrue( U.or( T ).isTrue() );
        assertTrue( T.or( T ).isTrue() );
    }

    @Test
    public void testImplies()
    {
        assertTrue( F.implies( F ).isTrue() );
        assertTrue( U.implies( F ).isUnknown() );
        assertTrue( T.implies( F ).isFalse() );

        assertTrue( F.implies( U ).isTrue() );
        assertTrue( U.implies( U ).isTrue() );
        assertTrue( U.implies( null ).isTrue() );
        assertTrue( T.implies( U ).isUnknown() );

        assertTrue( F.implies( T ).isTrue() );
        assertTrue( U.implies( T ).isTrue() );
        assertTrue( T.implies( T ).isTrue() );
    }

    @Test
    public void testToString()
    {
        assertEquals( T.toString(), "true" );
        assertEquals( F.toString(), "false" );
        assertEquals( U.toString(), "unknown" );
    }

    @Test
    public void testFromString()
    {
        assertTrue( fromString( "true" ).isTrue() );
        assertTrue( fromString( "false" ).isFalse() );
        assertTrue( fromString( "unknown" ).isUnknown() );
        assertTrue( fromString( null ).isUnknown() );
    }

    @Test
    public void testBooleanCoalesce()
    {
        assertTrue( coalesce( TRUE ).isTrue() );
        assertTrue( coalesce( FALSE ).isFalse() );
        assertTrue( coalesce( (Boolean) null ).isUnknown() );
    }

    @Test
    public void testTrileanCoalesce()
    {
        assertTrue( coalesce( T ).isTrue() );
        assertTrue( coalesce( F ).isFalse() );
        assertTrue( coalesce( U ).isUnknown() );
        assertTrue( coalesce( (Trilean) null ).isUnknown() );
    }

    @Test
    public void testStringCoalesce()
    {
        assertTrue( coalesce( "true" ).isTrue() );
        assertTrue( coalesce( " true " ).isTrue() );
        assertTrue( coalesce( "yes" ).isTrue() );
        assertTrue( coalesce( "false" ).isFalse() );
        assertTrue( coalesce( "false " ).isFalse() );
        assertTrue( coalesce( "no " ).isFalse() );
        assertTrue( coalesce( "unknown" ).isUnknown() );
        assertTrue( coalesce( "maybe" ).isUnknown() );
        assertTrue( coalesce( (String) null ).isUnknown() );
    }

    @Test
    public void testEquals()
    {
        assertTrue( Trilean.equals( null, "hello" ).isUnknown() );
        assertTrue( Trilean.equals( "hello", null ).isUnknown() );
        assertTrue( Trilean.equals( null, null ).isUnknown() );
        assertTrue( Trilean.equals( "hello", "hello" ).isTrue() );
        assertTrue( Trilean.equals( "bosh", "hello" ).isFalse() );
    }

    @Test
    public void testNotEquals()
    {
        assertTrue( Trilean.notEquals( null, "hello" ).isUnknown() );
        assertTrue( Trilean.notEquals( "hello", null ).isUnknown() );
        assertTrue( Trilean.notEquals( null, null ).isUnknown() );
        assertTrue( Trilean.notEquals( "hello", "hello" ).isFalse() );
        assertTrue( Trilean.notEquals( "bosh", "hello" ).isTrue() );
    }

    @Test
    public void assumeToBeTrueTest()
    {
        assertTrue( T.assumeToBeTrue().isTrue() );
        assertTrue( F.assumeToBeTrue().isFalse() );
        assertTrue( U.assumeToBeTrue().isTrue() );
    }

    @Test
    public void assumeToBeFalseTest()
    {
        assertTrue( T.assumeToBeFalse().isTrue() );
        assertTrue( F.assumeToBeFalse().isFalse() );
        assertTrue( U.assumeToBeFalse().isFalse() );
    }

    @Test
    public void isUnknownOrTrueTest()
    {
        assertTrue( T.isUnknownOrTrue() );
        assertFalse( F.isUnknownOrTrue() );
        assertTrue( U.isUnknownOrTrue() );
    }

    @Test
    public void isUnknownOrFalseTest()
    {
        assertFalse( T.isUnknownOrFalse() );
        assertTrue( F.isUnknownOrFalse() );
        assertTrue( U.isUnknownOrFalse() );
    }

}
