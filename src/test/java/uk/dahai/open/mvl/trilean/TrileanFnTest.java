package uk.dahai.open.mvl.trilean;

import org.junit.jupiter.api.Test;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static org.junit.jupiter.api.Assertions.*;
import static uk.dahai.open.mvl.trilean.Trilean.*;

public class TrileanFnTest
{
    @Test
    public void ternaryValueTest()
    {
        assertEquals( F.ternaryValue(), TrileanFn.toTernaryValue.apply( F ) );
        assertEquals( U.ternaryValue(), TrileanFn.toTernaryValue.apply( U ) );
        assertEquals( T.ternaryValue(), TrileanFn.toTernaryValue.apply( T ) );
    }

    @Test
    public void balancedTernaryValueTest()
    {
        assertEquals( F.balancedTernaryValue(), TrileanFn.toBalancedTernaryValue.apply( F ) );
        assertEquals( U.balancedTernaryValue(), TrileanFn.toBalancedTernaryValue.apply( U ) );
        assertEquals( T.balancedTernaryValue(), TrileanFn.toBalancedTernaryValue.apply( T ) );
    }

    @Test
    public void booleanValueTest()
    {
        assertEquals( F.booleanValue(), TrileanFn.toBoolean.apply( F ) );
        assertEquals( U.booleanValue(), TrileanFn.toBoolean.apply( U ) );
        assertEquals( T.booleanValue(), TrileanFn.toBoolean.apply( T ) );
    }


    @Test
    public void nullLogicTest()
    {
        assertFalse( TrileanFn.isFalse.test( null ) );
        assertTrue( TrileanFn.isUnknown.test( null ) );
        assertFalse( TrileanFn.isTrue.test( null ) );
        assertFalse( TrileanFn.isKnown.test( null ) );
    }

    @Test
    public void falsyLogicTest()
    {
        assertEquals( F.isFalse(), TrileanFn.isFalse.test( F ) );
        assertEquals( F.isTrue(), TrileanFn.isTrue.test( F ) );
        assertEquals( F.isNotTrue(), TrileanFn.isNotTrue.test( F ) );
        assertEquals( F.isNotFalse(), TrileanFn.isNotFalse.test( F ) );
        assertEquals( F.isKnown(), TrileanFn.isKnown.test( F ) );
        assertEquals( F.isUnknown(), TrileanFn.isUnknown.test( F ) );
        assertEquals( F.isUnknownOrTrue(), TrileanFn.isUnknownOrTrue.test( F ) );
        assertEquals( F.isUnknownOrFalse(), TrileanFn.isUnknownOrFalse.test( F ) );
    }

    @Test
    public void truthyLogicTest()
    {
        assertEquals( T.isFalse(), TrileanFn.isFalse.test( T ) );
        assertEquals( T.isTrue(), TrileanFn.isTrue.test( T ) );
        assertEquals( T.isKnown(), TrileanFn.isKnown.test( T ) );
        assertEquals( T.isUnknown(), TrileanFn.isUnknown.test( T ) );
        assertEquals( T.isNotTrue(), TrileanFn.isNotTrue.test( T ) );
        assertEquals( T.isNotFalse(), TrileanFn.isNotFalse.test( T ) );
        assertEquals( T.isUnknownOrTrue(), TrileanFn.isUnknownOrTrue.test( T ) );
        assertEquals( T.isUnknownOrFalse(), TrileanFn.isUnknownOrFalse.test( T ) );
    }

    @Test
    public void unknownLogicTest()
    {
        assertEquals( U.isFalse(), TrileanFn.isFalse.test( U ) );
        assertEquals( U.isTrue(), TrileanFn.isTrue.test( U ) );
        assertEquals( U.isKnown(), TrileanFn.isKnown.test( U ) );
        assertEquals( U.isUnknown(), TrileanFn.isUnknown.test( U ) );
        assertEquals( U.isNotTrue(), TrileanFn.isNotTrue.test( U ) );
        assertEquals( U.isNotFalse(), TrileanFn.isNotFalse.test( U ) );
        assertEquals( U.isUnknownOrTrue(), TrileanFn.isUnknownOrTrue.test( U ) );
        assertEquals( U.isUnknownOrFalse(), TrileanFn.isUnknownOrFalse.test( U ) );
    }


    @Test
    public void testNot()
    {
        assertEquals( T.not(), TrileanFn.not.apply( T ) );
        assertEquals( U.not(), TrileanFn.not.apply( U ) );
        assertEquals( U.not(), TrileanFn.not.apply( null ) );
        assertEquals( F.not(), TrileanFn.not.apply( F ) );
    }

    @Test
    public void testAnd()
    {
        assertEquals( F.and( F ), TrileanFn.and.apply( F, F ) );
        assertEquals( U.and( F ), TrileanFn.and.apply( U, F ) );
        assertEquals( U.and( F ), TrileanFn.and.apply( null, F ) );
        assertEquals( T.and( F ), TrileanFn.and.apply( T, F ) );

        assertEquals( F.and( U ), TrileanFn.and.apply( F, U ) );
        assertEquals( U.and( U ), TrileanFn.and.apply( U, U ) );
        assertEquals( U.and( U ), TrileanFn.and.apply( null, U ) );
        assertEquals( T.and( U ), TrileanFn.and.apply( T, U ) );

        assertEquals( F.and( T ), TrileanFn.and.apply( F, T ) );
        assertEquals( U.and( T ), TrileanFn.and.apply( U, T ) );
        assertEquals( U.and( T ), TrileanFn.and.apply( null, T ) );
        assertEquals( T.and( T ), TrileanFn.and.apply( T, T ) );

        assertEquals( U.and( U ), TrileanFn.and.apply( null, null ) );

    }

    @Test
    public void testAndWith()
    {
        assertEquals( F.and( F ), TrileanFn.andWith.apply( F ).apply( F ) );
        assertEquals( U.and( F ), TrileanFn.andWith.apply( U ).apply( F ) );
        assertEquals( U.and( F ), TrileanFn.andWith.apply( null ).apply( F ) );
        assertEquals( T.and( F ), TrileanFn.andWith.apply( T ).apply( F ) );

        assertEquals( F.and( U ), TrileanFn.andWith.apply( F ).apply( U ) );
        assertEquals( U.and( U ), TrileanFn.andWith.apply( U ).apply( U ) );
        assertEquals( U.and( U ), TrileanFn.andWith.apply( null ).apply( U ) );
        assertEquals( T.and( U ), TrileanFn.andWith.apply( T ).apply( U ) );

        assertEquals( F.and( T ), TrileanFn.andWith.apply( F ).apply( T ) );
        assertEquals( U.and( T ), TrileanFn.andWith.apply( U ).apply( T ) );
        assertEquals( U.and( T ), TrileanFn.andWith.apply( null ).apply( T ) );
        assertEquals( T.and( T ), TrileanFn.andWith.apply( T ).apply( T ) );

        assertEquals( U.and( U ), TrileanFn.andWith.apply( null ).apply( null ) );
    }

    @Test
    public void testOr()
    {
        assertEquals( F.or( F ), TrileanFn.or.apply( F, F ) );
        assertEquals( U.or( F ), TrileanFn.or.apply( U, F ) );
        assertEquals( U.or( F ), TrileanFn.or.apply( null, F ) );
        assertEquals( T.or( F ), TrileanFn.or.apply( T, F ) );

        assertEquals( F.or( U ), TrileanFn.or.apply( F, U ) );
        assertEquals( U.or( U ), TrileanFn.or.apply( U, U ) );
        assertEquals( U.or( U ), TrileanFn.or.apply( null, U ) );
        assertEquals( T.or( U ), TrileanFn.or.apply( T, U ) );

        assertEquals( F.or( T ), TrileanFn.or.apply( F, T ) );
        assertEquals( U.or( T ), TrileanFn.or.apply( U, T ) );
        assertEquals( U.or( T ), TrileanFn.or.apply( null, T ) );
        assertEquals( T.or( T ), TrileanFn.or.apply( T, T ) );

        assertEquals( U.or( U ), TrileanFn.or.apply( null, null ) );

    }

    @Test
    public void testOrWith()
    {
        assertEquals( F.or( F ), TrileanFn.orWith.apply( F ).apply( F ) );
        assertEquals( U.or( F ), TrileanFn.orWith.apply( U ).apply( F ) );
        assertEquals( U.or( F ), TrileanFn.orWith.apply( null ).apply( F ) );
        assertEquals( T.or( F ), TrileanFn.orWith.apply( T ).apply( F ) );

        assertEquals( F.or( U ), TrileanFn.orWith.apply( F ).apply( U ) );
        assertEquals( U.or( U ), TrileanFn.orWith.apply( U ).apply( U ) );
        assertEquals( U.or( U ), TrileanFn.orWith.apply( null ).apply( U ) );
        assertEquals( T.or( U ), TrileanFn.orWith.apply( T ).apply( U ) );

        assertEquals( F.or( T ), TrileanFn.orWith.apply( F ).apply( T ) );
        assertEquals( U.or( T ), TrileanFn.orWith.apply( null ).apply( T ) );
        assertEquals( T.or( T ), TrileanFn.orWith.apply( T ).apply( T ) );

        assertEquals( U.or( U ), TrileanFn.orWith.apply( null ).apply( null ) );

    }

    @Test
    public void testImplies()
    {
        assertEquals( F.implies( F ), TrileanFn.implies.apply( F, F ) );
        assertEquals( U.implies( F ), TrileanFn.implies.apply( U, F ) );
        assertEquals( U.implies( F ), TrileanFn.implies.apply( null, F ) );
        assertEquals( T.implies( F ), TrileanFn.implies.apply( T, F ) );

        assertEquals( F.implies( U ), TrileanFn.implies.apply( F, U ) );
        assertEquals( U.implies( U ), TrileanFn.implies.apply( U, U ) );
        assertEquals( U.implies( U ), TrileanFn.implies.apply( null, null ) );
        assertEquals( T.implies( U ), TrileanFn.implies.apply( T, U ) );

        assertEquals( F.implies( T ), TrileanFn.implies.apply( F, T ) );
        assertEquals( U.implies( T ), TrileanFn.implies.apply( U, T ) );
        assertEquals( U.implies( T ), TrileanFn.implies.apply( null, T ) );
        assertEquals( T.implies( T ), TrileanFn.implies.apply( T, T ) );

    }

    @Test
    public void testItImplies()
    {
        assertEquals( F.implies( F ), TrileanFn.itImplies.apply( F ).apply( F ) );
        assertEquals( U.implies( F ), TrileanFn.itImplies.apply( U ).apply( F ) );
        assertEquals( U.implies( F ), TrileanFn.itImplies.apply( null ).apply( F ) );
        assertEquals( T.implies( F ), TrileanFn.itImplies.apply( T ).apply( F ) );

        assertEquals( F.implies( U ), TrileanFn.itImplies.apply( F ).apply( U ) );
        assertEquals( U.implies( U ), TrileanFn.itImplies.apply( U ).apply( U ) );
        assertEquals( U.implies( U ), TrileanFn.itImplies.apply( null ).apply( null ) );
        assertEquals( T.implies( U ), TrileanFn.itImplies.apply( T ).apply( U ) );

        assertEquals( F.implies( T ), TrileanFn.itImplies.apply( F ).apply( T ) );
        assertEquals( U.implies( T ), TrileanFn.itImplies.apply( U ).apply( T ) );
        assertEquals( U.implies( T ), TrileanFn.itImplies.apply( null ).apply( T ) );
        assertEquals( T.implies( T ), TrileanFn.itImplies.apply( T ).apply( T ) );
    }

    @Test
    public void testToString()
    {
        assertEquals( T.toString(), TrileanFn.toString.apply( T ) );
        assertEquals( F.toString(), TrileanFn.toString.apply( F ) );
        assertEquals( U.toString(), TrileanFn.toString.apply( U ) );
        assertEquals( U.toString(), TrileanFn.toString.apply( null ) );
    }

    @Test
    public void testFromString()
    {
        assertEquals( fromString( "true" ), TrileanFn.fromString.apply( "true" ) );
        assertEquals( fromString( "false" ), TrileanFn.fromString.apply( "false" ) );
        assertEquals( fromString( "unknown" ), TrileanFn.fromString.apply( "unknown" ) );
        assertEquals( fromString( "unknown" ), TrileanFn.fromString.apply( null ) );
    }

    @Test
    public void testBooleanCoalesce()
    {
        assertEquals( coalesce( TRUE ), TrileanFn.coalesce.apply( TRUE ) );
        assertEquals( coalesce( FALSE ), TrileanFn.coalesce.apply( FALSE ) );
        assertEquals( coalesce( (Boolean) null ), TrileanFn.coalesce.apply( null ) );
    }

    @Test
    public void testTrileanCoalesce()
    {
        assertEquals( coalesce( T ), TrileanFn.coalesceTrilean.apply( T ) );
        assertEquals( coalesce( F ), TrileanFn.coalesceTrilean.apply( F ) );
        assertEquals( coalesce( U ), TrileanFn.coalesceTrilean.apply( U ) );
        assertEquals( coalesce( (Trilean) null ), TrileanFn.coalesceTrilean.apply( null ) );
    }

}
